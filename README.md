# qingya-yii2rpc-client

#### 介绍

基于hprose封装的yii2使用的rpc客户端

#### 软件架构

软件架构说明

#### 安装教程

```shell
composer require qingya/yii2rpc-server
```

#### 使用说明

1. env必须配置的参数，注意Yii框架配置等号前不能有空格，等号后的空格也是值的一部分

```ini
rpcServer.center=服务中心地址              #必须

#调试时使用，指定某个服务项目的服务地址，也可以直接通过client初始化设置
rpcServer.服务项目标识=对外的服务地址       #可选 配置后，请求指定的项目，优先走env配置，一般走服务中心获取

rpcServer.centerCacheTime=300             #可选 rpc服务中心请求缓存时间，默认300秒，0=不缓存

rpcClient.appKey=clientAppKey             #必须 客户端发起请求的appKey配置
rpcClient.appSecret=clientAppSecret       #必须 客户端发起请求的appSecret配置
rpcClient.system=default                  #当前项目标识（rpc中识别），建议入口文件做常量配置
rpcLockDriver=yii                         #缓存锁驱动，默认yii的缓存组件，推荐配置redis
```

2. 入口文件必须定义常量
```shell
defined('YII_START_TIME') or define('YII_START_TIME', microtime(true));
```

#### 特别注意

客户端拼接完成的rpc服务请求url格式化：http://www.xxx.com/rpc/serviceName
serviceName：对应服务端的rpc服务类名称，首字母大写

分版本请请求实现 通过header参数serviceversion区分要请求的目标服务版本

#### 参与贡献

#### 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
