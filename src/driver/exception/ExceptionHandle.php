<?php

namespace QingYa\Yii2RpcServer\driver\exception;

use ArgumentCountError;
use Cassandra\Exception\ValidationException;
use QingYa\Yii2RpcServer\constants\RpcSystemCodeConst;
use Error;
use Exception;
use yii\base\ErrorHandler;

/**
 * 异常处理、记录异常日志
 * Class ExceptionHandle
 * @package QingYa\Yii2RpcServer\driver\exception
 */
class ExceptionHandle extends ErrorHandler
{

    /**
     * Report or log an exception.
     *
     * @param Exception $e
     * @return void
     */
    public function report(Exception $e)
    {
        $this->reportHandle($e);//记录错误日志
        $this->renderHandle($e);
    }

    public function reportHandle($e)
    {
        if (($e instanceof Error)) {
            // 收集异常数据
            $data = [
                'method' => \Yii::$app->request->headers->get('method'),
                'msg'    => $e->getMessage(),
                'code'   => $e->getCode(),
                'file'   => str_replace('@rootPath', '', $e->getFile()),
                'line'   => $e->getLine(),
            ];
            // writeLog($data, 'exception');
        }
    }

    protected function renderException($e)
    {
        return $this->renderHandle($e);
    }

    public function render(Exception $e)
    {
        return $this->renderHandle($e);
    }

    public function renderHandle($e)
    {
        $msg = $e->getMessage();
        if ($e instanceof ValidationException) {
            $code = RpcSystemCodeConst::$validateFailed['code'];
        } else if ($e instanceof ArgumentCountError) {
            $code = RpcSystemCodeConst::$argCountFailed['code'];
            $msg  = RpcSystemCodeConst::$argCountFailed['msg'] . "({$msg})";
        } else {
            $code = $e->getCode() ?: RpcSystemCodeConst::$exception['code'];
        }
        if (stripos($e->getFile(), 'src/Hprose/Service.php') && stripos($msg, "Can\'t find this function") !== false) {
            $msg  = RpcSystemCodeConst::$methodNotFound['msg'];
            $code = RpcSystemCodeConst::$methodNotFound['code'];
        }
        $runtime  = defined('YII_START_TIME') ? round(microtime(true) - YII_START_TIME, 10) : 0;
        $usedtime = number_format($runtime, 6);
        $data     = [
            'code'              => $code,
            'msg'               => $msg,
            'time'              => time(),
            'used_time_server'  => $usedtime,
            'used_time_request' => 0,
            'used_time_total'   => $usedtime,
            'from'              => 'rpc server',
            'data'              => [],
        ];
        $httpCode = 200;
        $headers  = \Yii::$app->request->headers;
        if (!in_array($code, [100002, 100003, 100005, 100006, 100010, 100011, 100012, 100013, 100014, 100015, 100100])) {
            $detail               = [];
            $detail['file']       = str_replace('@rootPath', '', $e->getFile());
            $detail['line']       = $e->getLine();
            $detail['rpc_server'] = $headers->get('host') . \Yii::$app->request->getPathInfo() . '@' . $headers->get('method');
            $data['error_info']   = $detail;
            //判断yii常见的系统异常
            // if (stripos($detail['file'], '/yii/') !== false) {
            //     $httpCode = 500;
            // }
        }
        $response          = \Yii::$app->response;
        $response->content = $data;
        $response->setStatusCode($httpCode);
        $response->format = $response::FORMAT_JSON;
        return $response;
    }
}
