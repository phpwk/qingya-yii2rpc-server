<?php


namespace QingYa\Yii2RpcServer\filter;


use Hprose\Filter;
use stdClass;
use Exception;

/**
 * gzip数据压缩过滤器
 * Class FilterGzip
 * @package QingYa\Yii2RpcServer\rpcServer
 */
class FilterGzip implements Filter
{


    public function inputFilter($data, stdClass $context)
    {
//        $first = substr($data, 0, 1);
//        if (in_array($first, ['{']) || preg_match('@^[\w\[\]\{\}\@]+$@iUs', substr($data, -3)) || stripos($data, 'code') !== false) {
//            $dataNew = $data;
//        } else {
//            $dataNew = gzdecode($data);
//        }
        $dataNew                    = $data;
        $context->userData['input'] = $dataNew;
        return $dataNew;
    }

    public function outputFilter($data, stdClass $context)
    {
//        try {
//            return gzencode($data);
//        } catch (Exception $e) {
//            return $data;
//        }
        return $data;
    }

}