<?php


namespace QingYa\Yii2RpcServer\filter;


use Hprose\Filter;
use stdClass;

/**
 * 计算签名
 * Class FilterSign
 * @package QingYa\Yii2RpcServer\rpcServer
 */
class FilterSign implements Filter
{

    protected $appKey    = '';
    protected $appSecret = '';

    /**
     * FilterSign constructor.
     * @param $appKey
     * @param $appSecret
     */
    public function __construct($appKey, $appSecret)
    {
        $this->appKey    = $appKey;
        $this->appSecret = $appSecret;
    }

    public function inputFilter($data, stdClass $context)
    {
        return $data;
    }

    public function outputFilter($data, stdClass $context)
    {
        return $data;
    }

    public function makeSign($appkey, $appSecret, $path, $time, $rand, $dataStr)
    {
        $signStr = $appkey . '-' . $appSecret . '-' . $path . '-' . $time . '-' . $rand . '-' . $dataStr;
        return [
            'sign_str' => $signStr,
            'sign'     => md5($signStr),
        ];
    }


}