<?php


namespace QingYa\Yii2RpcServer\constants;

/**
 * 当前项目错误码
 * 规则 1-9999
 * Class ConstProjectCode
 * @package QingYa\Yii2RpcServer\constants
 */
class ConstProjectCode
{
    public static $demoError = [
        'msg'  => '项目错误',
        'code' => 0,
        'faq'  => '提示',
    ];

}