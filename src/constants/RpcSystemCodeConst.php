<?php


namespace QingYa\Yii2RpcServer\constants;

/**
 * Class ConstSystemCode
 * @package QingYa\Yii2RpcServer\rpcServer
 */
class RpcSystemCodeConst
{

    /**
     * @var array 成功
     */
    public static $success = [
        'msg'  => 'success',
        'code' => 200,//根据实际业务需要调整
        'faq'  => '',
    ];

    /**
     * @var array 失败
     */
    public static $error = [
        'msg'  => '系统繁忙',
        'code' => 100000,
        'faq'  => '请稍后重试',
    ];

    public static $exception = [
        'msg'  => '系统异常',
        'code' => 100001,
        'faq'  => '查看系统异常日志',
    ];

    public static $signFailed = [
        'msg'  => '签名验证失败',
        'code' => 100002,
        'faq'  => '更新最新sdk',
    ];

    public static $validateFailed = [
        'msg'  => '数据验证失败',
        'code' => 100003,
        'faq'  => '检查请求参数是否符合条件',
    ];

    public static $argCountFailed = [
        'msg'  => '缺少必要参数',
        'code' => 100004,
        'faq'  => '检查远程调用参数数量',
    ];

    public static $ipWhiteFailed = [
        'msg'  => '客户端ip不在白名单',
        'code' => 100005,
        'faq'  => '把ip加入白名单系统',
    ];

    public static $responseFormatFailed = [
        'msg'  => '响应数据格式不正常',
        'code' => 100006,
        'faq'  => '检查返回数据格式',
    ];

    public static $serviceNotFound = [
        'msg'  => '服务不存在',
        'code' => 100007,
        'faq'  => '检查rpc服务端和客户端调用的服务是否完全一致',
    ];

    public static $methodNotFound = [
        'msg'  => '服务中方法不存在',
        'code' => 100008,
        'faq'  => '检查rpc服务端和客户端调用的方法是否完全一致',
    ];


    public static $secretError = [
        'msg'  => 'APPKEY或者密钥错误',
        'code' => 100011,
        'faq'  => '检查appkey或者密钥是否和rpc服务中心的一致',
    ];

    public static $dataNotFound = [
        'msg'  => '数据记录不存在',
        'code' => 100012,
        'faq'  => '',
    ];

    public static $noAccess = [
        'msg'  => '数据无访问权限',
        'code' => 100013,
        'faq'  => '',
    ];

    public static $signRepeat = [
        'msg'  => '重复请求',
        'code' => 100014,
        'faq'  => '可能超时时间设置的太短',
    ];

    public static $dataDisabled = [
        'msg'  => '数据被禁用',
        'code' => 100015,
        'faq'  => '',
    ];

    public static $clientError = [
        'msg'  => '客户端异常',
        'code' => 100100,
        'faq'  => '查看rpc客户端异常日志',
    ];


}