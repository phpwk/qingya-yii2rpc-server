<?php

namespace QingYa\Yii2RpcServer;

use ArgumentCountError;


use QingYa\Helper\EnvUtils;
use QingYa\Helper\MyLockUtils;
use QingYa\Yii2RpcClient\RpcCenterClient;
use QingYa\Yii2RpcServer\constants\RpcSystemCodeConst;
use QingYa\Yii2RpcServer\driver\exception\ExceptionHandle;
use QingYa\Yii2RpcServer\filter\FilterGzip;
use QingYa\Yii2RpcServer\filter\FilterSign;
use Error;
use Exception;
use QingYa\Yii2RpcServer\Hprose\Yii\Server as HproseServer;
use stdClass;

/**
 * Class Server
 * @package Dhcc\RpcServer
 */
class Server extends \yii\web\Controller
{

    public $enableCsrfValidation = false;

    /**
     * 服务端版本
     */
    const VERSION_SERVER     = 'v1.0.0';
    const SERVICE_CLASS_ROOT = 'frontend\\modules\\rpc\\';//rpc服务根目录

    const SERVICE_CLASS_DIR    = 'controllers';//可以切换到controllers，也可以统一走配置处理不用修改server
    const SERVICE_CLASS_SUFFIX = 'Controller';//服务类统一后缀: Service、Controller

    // const SERVICE_CLASS_DIR    = 'services';//可以切换到controllers，也可以统一走配置处理不用修改server
    // const SERVICE_CLASS_SUFFIX = 'Service';//服务类统一后缀: Service、Controller

    /**
     * 服务启动入口
     * @return string|void
     * @throws Exception
     */
    public function start()
    {
        @header('Rpc-Server: ' . self::VERSION_SERVER);
        $pathInfo = $this->request->getPathInfo();// rpc/Center
        $path     = substr($pathInfo, 4);//根据项目实际路径处理
        $path     = str_replace('//', '/', $path);

        // 初始化服务目录
        // $this->serviceDirInit();
        // 初始化demo
        // $this->demoInit();

        if (!$path || $path == '/' || stripos($path, '.') || strlen($path) > 50 || !preg_match('@^[a-zA-Z0-9/_]+$@iUs', $path)) {
            return 'rpc-server is running';
        }
        $path = trim($path, '/');
        writeLog("path：{$path}", 'server');
        $class = $this->getClass($path);
        writeLog("className:" . $class, 'server');
        return $this->service($class);
    }

    /**
     * 获取当前请求的类
     * @param $path
     * @return string
     */
    protected function getClass($path): string
    {
        $classDir  = self::SERVICE_CLASS_ROOT . self::SERVICE_CLASS_DIR . '\\';
        $className = strpos($path, '/') === false ? ucfirst(str_replace('/', '\\', $path)) : $path;
        return $classDir . $className . self::SERVICE_CLASS_SUFFIX;
    }

    /**
     * rpc服务
     * @param $serviceClass
     * @return mixed
     * @throws Exception
     */
    protected function service($serviceClass)
    {
        // writeLog("service start", 'server');
        $server = new HproseServer();
        if (!class_exists($serviceClass)) {
            // writeLog("class_exists_not : {$serviceClass}", 'server');
            throw new Exception(RpcSystemCodeConst::$serviceNotFound['msg'], RpcSystemCodeConst::$serviceNotFound['code']);
        }
        // 需要注意 这里如果添加多个过滤器时候，执行顺序是倒序
        $server->addFilter(new FilterGzip());
        // 暴露服务
        $server->addInstanceMethods(new $serviceClass);
        // 验证签名
        $server->onBeforeInvoke = function ($name, $args, $byref, stdClass $context) {
            $headers     = $this->request->headers;
            $realProject = $headers->get('realproject');
            writeLog("验证签名 start", 'server');
            //请求到rpc服务中心时不验证签名，rpc服务中心只提供两个接口服务，后期使用项目标识判断:SYSTEM == 'rpc-center'
            if ($realProject == 'rpc-center') {
                return true;
            }

            writeLog("验证签名 start2", 'server');
            if ($headers->get('realproject') == $headers->get('system')) {
                //throw new Exception("RPC项目(" . $headers->get('realproject') . ")禁止自己调用自己", RpcSystemCodeConst::$exception['code']);
            }
            writeLog("realproject:" . $headers->get('realproject'), 'server');
            // 根据客户端提供的appkey，当前项目标识，通过rpc-center获取 当前appkey是否被授权，允许的话返回加密密钥，缓存，生成签名（成功后缓存10分钟）
            $appKey = $headers->get('appkey');
            RpcCenterClient::getClientFromServer('rpc-server-RI7TGZu6pS', 'DqNBbBNM952SGRqA3R3eWv6Qsxx2tSi2');
            // RpcCenterClient::getClientFromServer();
            writeLog("salt:" . $headers->get('salt'), 'server');
            $accessInfo = RpcCenterClient::getProjectInfoForServer($headers->get('realproject'), $headers->get('salt'), $appKey);
            writeLog($accessInfo, 'server');
            if ($accessInfo['code'] !== 200) {
                throw new Exception($accessInfo['msg'], $accessInfo['code']);
            } else {
                $appSecretHash = $accessInfo['data']['hash'] ?? '';//appSecret MD5
            }

            $clientSign = $headers->get('sign');
            $filterSign = new FilterSign($appKey, $appSecretHash);
            $path       = $this->request->getUrl();
            writeLog("appSecretHash:{$appSecretHash}", 'server');
            $signArray  = $filterSign->makeSign($appKey, $appSecretHash, $path, $headers->get('time'), $headers->get('rand'), $context->userData['input']);
            $serverSign = $signArray['sign'];
            if ($headers->get('signdebug')) {
                $msg = "签名调试：" . ($signArray['sign_str'] === $headers->get('signclient') ? '通过' : '不通过') . "\n";
                $msg .= 'sign_server:' . preg_replace('@-(.*)-@iUs', '-服务端密钥-', $signArray['sign_str'], 1) . "\n";
                $msg .= 'sign_client:' . preg_replace('@-(.*)-@iUs', '-客户端密钥-', $headers->get('signclient'), 1) . "\n";
                // $msg .= 'rpcClientAppSecret:' . env('rpcClientAppSecret') . "\n";
                // $msg .= 'rpcClientAppSecret-md5:' . md5(env('rpcClientAppSecret')) . "\n";
                // $msg .= 'appSecretHash-secret:' . $accessInfo['data']['secret'] . "\n";
                // $msg .= 'appSecretHash:' . $appSecretHash . "\n";
                throw new Exception($msg, RpcSystemCodeConst::$signFailed['code']);
            }
            if (
                empty($headers->get('sign')) || empty($headers->get('time')) || empty($headers->get('rand')) ||
                ($clientSign !== $serverSign)
            ) {
                throw new Exception(RpcSystemCodeConst::$signFailed['msg'], RpcSystemCodeConst::$signFailed['code']);
            }
            $signKey = "sign:{$clientSign}";
            //缓存锁使用的驱动，默认走yii自带的缓存配置，推荐使用redis(需要配置)
            $driver = EnvUtils::get('rpcLockDriver', 'yii');
            if (!$headers->get('isselect', false) && !MyLockUtils::lock($signKey, 300, $driver)) {
                throw new Exception(RpcSystemCodeConst::$signRepeat['msg'], RpcSystemCodeConst::$signRepeat['code']);
            }
        };

        $headers = $this->request->headers;
        // 方便日志记录方法名
        if ($headers->get('method')) {
            $_SERVER['REQUEST_URI'] .= "@{$headers->get('method')}";
        }

        writeLog("异常处理 start", 'server');
        // 异常处理
        $server->onSendError = function ($e, stdClass $context) {
            // 收集异常数据
            $exceptionHandle = new ExceptionHandle();
            if ($e instanceof ArgumentCountError) {
                $exceptionHandle->reportHandle($e);
                $data = $exceptionHandle->renderHandle($e);
            } else if ($e instanceof Error) {
                $exceptionHandle->reportHandle($e);
                $data = $exceptionHandle->renderHandle($e);
            } else {
                $exceptionHandle->report($e);
                $data = $exceptionHandle->render($e);
            }
            throw new Exception(json_encode($data->content));
        };
        writeLog("异常处理 end", 'server');
        return $server->start();
    }

}