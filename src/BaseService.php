<?php

namespace QingYa\Yii2RpcServer;


use QingYa\Yii2RpcServer\constants\RpcSystemCodeConst;
use yii\web\Request;

/**
 * 服务基类
 * Class BaseService
 * @package QingYa\Yii2RpcServer
 */
class BaseService
{

    /**
     * 是否批量验证
     * @var bool
     */
    protected $batchValidate = false;
    /**
     * @var bool 验证失败是否抛出异常
     */
    protected $failException = true;

    /**
     * @var array rpc响应数据
     */
    protected $responseData = [
        'code'              => 0,
        'msg'               => 'success',
        'time'              => 0,
        'used_time_server'  => 0,
        'used_time_request' => 0,
        'used_time_total'   => 0,
        'reqs'              => 0,
        'from'              => 'rpc server',
        'data'              => [
        ],
    ];

    protected $clientAppKey    = '';
    protected $clientHospID    = 0;
    protected $clientProjectId = 0;
    protected $projectId       = 0;
    protected $nocache         = false;
    protected $isSelect        = false;
    /**
     * @var \yii\console\Request|Request
     */
    protected $request;

    public function __construct()
    {
        $this->request = \Yii::$app->request;
        $header        = $this->request->headers;
        // writeLog("request-headers", 'rpc-server');
        // writeLog($header->toArray(), 'rpc-server');
        //参数获取对应客户端的header设置，可自定义添加删除
        $this->clientAppKey = $header->get('appkey');
        $this->clientHospID = $header->get('hospid');
        $this->nocache      = boolval($header->get('nocache'));
        $this->isSelect     = boolval($header->get('isselect'));
        $this->projectId    = $header->get('projectid');
    }


    /**
     * 成功返回方法
     * @param array  $data
     * @param string $msg
     * @return array $responseData
     */
    protected function success(array $data = [], string $msg = ''): array
    {
        $msg = $msg ?: RpcSystemCodeConst::$success['msg'];
        return $this->response(RpcSystemCodeConst::$success['code'], $msg, $data);
    }

    /**
     * 失败返回方法
     * @param        $constError
     * @param string $customMsg
     * @param array  $data
     * @return array
     */
    protected function error($constError, string $customMsg = '', array $data = []): array
    {
        $code = $constError['code'];
        $msg  = $customMsg ?: $constError['msg'];
        if ($code < 10000) {
            $code = (200 + $this->projectId) * 10000 + $code;
        }
        return $this->response($code, $msg, $data);
    }

    /**
     * response统一格式返回
     * @param $code
     * @param $msg
     * @param $data
     * @return array
     */
    private function response($code, $msg, $data): array
    {
        /**
         * 计算耗时
         */
        $runTime  = round(microtime(true) - YII_START_TIME, 10);
        $usedTime = number_format($runTime, 6);
        /**
         * 计算reqs
         */
        $reqs                                   = $runTime > 0 ? number_format(1 / $runTime, 2) : '∞';
        $this->responseData['code']             = $code;
        $this->responseData['msg']              = $msg;
        $this->responseData['data']             = $data;
        $this->responseData['time']             = time();
        $this->responseData['used_time_server'] = $usedTime;
        $this->responseData['used_time_total']  = $usedTime;
        $this->responseData['reqs']             = $reqs;
        // 增加统计日志
        $saveLog = [
            'used_time'    => $usedTime,
            'reqs'         => $reqs,
            'memory_usage' => round(memory_get_peak_usage(true) / 1024 / 1024, 2),//内存占用,单位为M
        ];
        if (stripos($_SERVER['REQUEST_URI'], '/rpc/') !== false) {
            // writeLog($saveLog, 'rpc-server');
        }
        // writeLog($this->responseData, 'rpc-server');
        return $this->responseData;
    }

    /**
     * 验证数据
     * @access protected
     * @param array        $data     数据
     * @param string|array $validate 验证器名或者验证规则数组
     * @param array        $message  提示信息
     * @param bool         $batch    是否批量验证
     * @param mixed        $callback 回调方法（闭包）
     * @return array|string|true
     */
    protected function validate($data, $validate, $message = [], $batch = false, $callback = null)
    {
        // if (验证失败) {
        //     return $this->error(RpcSystemCodeConst::$validateFailed, $v->getError());
        // }
        return true;
    }

}