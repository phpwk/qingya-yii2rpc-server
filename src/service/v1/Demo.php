<?php

namespace QingYa\Yii2RpcServer\service\v1;

use QingYa\Yii2RpcServer\BaseService;
use QingYa\Yii2RpcServer\constants\RpcSystemCodeConst;

/**
 * 服务实例
 * @ApiSummary    (描述)
 * @ApiWeigh      (1)
 * Class Demo
 * @package QingYa\Yii2RpcServer\service\v1
 */
class Demo extends BaseService
{

    /**
     * 测试方法
     * @ApiSummary        (测试描述，该注释根据实际项目需要调整，方便api文档自动生成)
     * @ApiWeigh          (100)
     * @ApiReturnParams   (name="test1", type="string", description="测试返回1", sample="123")
     * @ApiReturnParams   (name="test2", type="string", description="测试返回2", sample="456")
     * @return array
     */
    public function test()
    {
        $data = [
            'test1' => '123',
            'test2' => '456',
        ];
        return $this->success($data);
    }

}